Pour pouvoir installer tout les modules tryton officiel

python list_modules.py >requirements.txt

docker run -d --name db --expose 5432 -e POSTGRES_PASSWORD=password postgres

docker run --name tryton --link db:db -e POSTGRES_PASSWORD=password -p 8000:8000 -i -t libreit/tryton
