import urllib2
import re
res = urllib2.urlopen("http://hg.tryton.org/modules/?sort=name")
content = res.read()
pattern = '<td><a href="/modules/(.*)/">'
list_modules = re.findall(pattern, content)
for module in list_modules:
    print "trytond_" + module + "==3.8"
